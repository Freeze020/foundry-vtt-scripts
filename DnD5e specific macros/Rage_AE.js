// v10!
if(!token) return;
const rageResourceName = "Rage - Charges";
const effectLabel = "Rage";
let hasAvailableResource = token.actor.type === "npc" ? true : false;
const level = token.actor.type === "npc" ? actor.system.details.cr : actor.items.find(i=> i.name === "Barbarian" && i.type === "class").system.levels;
const gameRound = game.combat ? game.combat.round : 0;
let message = "";
const effect = token.actor.effects.find(e => e.label === effectLabel);

if (effect) {
    let rageId = effect.id;
    await token.actor.deleteEmbeddedDocuments("ActiveEffect", [rageId]);
    message = `<i>${actor.name} is no longer raging.</i>`;
} 
else {
    let resourceKey = "";
    if(actor.type === "character"){
        resourceKey = Object.keys(token.actor.system.resources).find(k => token.actor.system.resources[k].label === `${rageResourceName}`);
        if (resourceKey && token.actor.system.resources[resourceKey].value > 0) hasAvailableResource = true;
        if(!hasAvailableResource) return ui.notifications.warn("You are out of charges to Rage.");
    }
    const effectData = {
        label : effectLabel,
        icon : "icons/skills/wounds/injury-face-impact-orange.webp",
        changes: [
            {
                "key": "system.bonuses.mwak.damage",
                "value": `${(Math.ceil(Math.floor(level/(9-(Math.floor(level/9)))+2)))}`,
                "mode": 2,
                "priority": "20"
            },
            {
                "key": "system.traits.dr.value",
                "value": "slashing",
                "mode": 2,
                "priority": "20"
            },
            {
                "key": "system.traits.dr.value",
                "value": "bludgeoning",
                "mode": 2,
                "priority": "20"
            },
            {
                "key": "system.traits.dr.value",
                "value": "piercing",
                "mode": 2,
                "priority": "20"
            },
            {
                "key": "flags.midi-qol.advantage.ability.save.str",
                "value": "1",
                "mode": 0,
                "priority": "20"
            },
            {
                "key": "flags.midi-qol.advantage.ability.check.str",
                "value": "1",
                "mode": 0,
                "priority": "20"
            }
        ],
        disabled: false,
        duration: {rounds: (level > 14 ? 1000 : 10), seconds: (level > 14 ? 6000 : 60),startRound: gameRound, startTime: game.time.worldTime},
    };
    await token.actor.createEmbeddedDocuments("ActiveEffect", [effectData]);
    message = `<i>${actor.name} is RAAAAAAAGING</i>`;
    if(actor.type === "character"){
        let newResources = duplicate(actor.system.resources);
        newResources[resourceKey].value--;
        await actor.update({"system.resources": newResources});
    }
    
}
ChatMessage.create({
    user: game.user.id,
    speaker: ChatMessage.getSpeaker({actor}),
    content: message,
    type: CONST.CHAT_MESSAGE_TYPES.EMOTE
});
