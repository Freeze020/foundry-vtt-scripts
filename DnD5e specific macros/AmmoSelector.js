// item macro that fires before the item is rolled.
// item is defined by the ItemMacro module, token is defined as the controlled token.
// for 3.1+
const ammoType = item.system.type.baseItem.includes("crossbow") ? "crossbowBolt" : 
                 item.system.type.baseItem.includes("sling") ? "slingBullet" :
                 item.system.type.baseItem.includes("blowgun") ? "blowgunNeedle" : "arrow";

const ammoChoices = token.actor.items.filter(i => i.system.type?.subtype === ammoType && i.system.quantity > 0)
    .sort((a,b)=> a.system.price.value - b.system.price.value)
    .reduce((acc, i) => acc += `<option value="${i.id}">${i.name} | ${i.system.quantity} in the quiver`,``);

const content = `<form>
                    <div class="form-group">
                        <label>Ammo: </label>
                        <div class="form-fields">
                            <select name="ammoId">${ammoChoices}</select>
                        </div>
                    </div>
                </form>`;
new Dialog({
    title: "Ammo selector",
    content,
    buttons: {
        fire: {
            label: "Fire!",
            callback: async ([html]) => {
                const {ammoId} = new FormDataExtended(html.querySelector("form")).object;
                await item.update({"system.consume.target": ammoId});
                await item.use();
            }
        },
        cancel: {
            label: "Cancel"
        }
    }
}).render(true);