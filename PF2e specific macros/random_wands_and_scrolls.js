/*
Simple Wand/Scroll Generator
Input Type, Rank, Tradition, Quantity, Rarity, Output Type, and Mystification.
This will also create useable Specialty Wands for:
* Wand of Continuation
* Wand of LegerDemain
* Wand of Reaching
* Wand of Widening

Hit Ok and it will generate output.

When Generated Loot Actor is selected it will create a loot actor named Generated Loot 
if not available then populate this actor. If the actor is already available, it will 
populate the actor.
When Party Actor or Existing Loot Actor are selected, if only one actor of that type is 
available, it will populate that actor. If more are available another dialog will 
prompt for an actor choice.
*/

// quick function to create Uuids from the item id arrays.
function createUuids(arr){
  return arr.reduce((acc,e,i)=> {
    acc[i+1]=`Compendium.pf2e.equipment-srd.Item.${e}`; 
    return acc;
  } ,{});
}

// quick function to give the right uuid list based on the consumable that is selected.
function getUuidList(consumable){
  return {
      scroll: scrollUuids,
      wand: wandUuids,
      cont: specWandsUUIDs.cont,
      reach: specWandsUUIDs.reach,
      wide: specWandsUUIDs.wide,
      leger: specWandsUUIDs.leger
    }[consumable];
}

// creates the options for the onRender function.
function createRanks(obj){
  return Object.keys(obj).reduce((acc,e)=>{
    acc += `<option value="${e}">${game.i18n.format("PF2E.SpellLevelLabel")} ${e}</option>`;
    return acc;
  },"");
}

// on render function to populate the ranks drop down based on the consumable.
function onRender(event,html){
  html.querySelectorAll(".form-footer > button").forEach(e => e.style.minWidth = "50px");
  const selectCons = html.querySelector("select[name=consumable]");
  const selectRanks = html.querySelector("select[name=rank]");
  selectRanks.innerHTML = createRanks(scrollUuids);
  selectCons.addEventListener("change", ()=>{
    selectRanks.innerHTML = createRanks(getUuidList(selectCons.value)); 
  });
}

// the function that creates the dialog based on the constants noted below.
async function creationDialog(content){
  return await DialogV2.wait({
    window: {title: `Wand and Scroll Generator`},
    position: {width: 350},
    content,
    buttons: [{
        label: "Create",
        action: "create",
        callback: (event,button) => new FormDataExtended(button.form).object
      },{
        label: "Cancel",
        action: null
    }],
    rejectClose: false,
    render: onRender
  });
}

// makes the data for a random spell + consumable type.
async function getItem(rank, tradition, rarity, consumable){
  const compendiums = ["pf2e.spells-srd",/*"pf2e-expansion-pack.Expansion-Spells"*/];
  const packs = game.packs.filter(c => compendiums.includes(c.collection));
  let spells = [];
  for(let p of packs) {
    const idx = (await p.getIndex({fields: ["system.level.value","system.slug","system.traits","system.ritual","uuid","system.area","system.duration","system.range","system.time"]})).filter(f => {
      if(consumable === "wide" && !((f.system.time.value === "1"  || f.system.time.value === "2") || !!f.system.duration.value || !(f.system.area?.value >= 10 && (f.system.area?.type === "line" || f.system.area?.type === "burst" || f.system.area?.type === "cone")))) return false;
      if(consumable === "cont" && !((f.system.time.value === "1"  || f.system.time.value === "2") || !(f.system.duration.value === "1 hour"  || f.system.duration.value === "10 minutes"))) return false;
      if(consumable === "reach" && !((f.system.time.value === "1" || f.system.time.value === "2") || !(f.system.range?.value.includes("feet" || "touch")))) return false;
      return !f.system.traits.value.includes("cantrip") && !(f.system.ritual ??= false) && !f.system.traits.value.includes("focus") && f.system.level.value === rank && (f.system.traits.rarity === rarity || rarity === "any") && (f.system.traits.traditions.includes(tradition) || tradition === "random");
    });
    spells = spells.concat(idx);
  }
  const randomSpell = spells[Math.floor(Math.random()*spells.length)];
  if(!randomSpell) return null;
  const output = {
    name: `${game.i18n.format(consumableList[consumable])} ${(consumable === "wand" || consumable === "scroll") ? "of " : ""}${randomSpell.name} (Rank ${rank})`,
    uuid: randomSpell.uuid,
    suuid: getUuidList(consumable)[rank],
    level: rank
  };
  return output;
}

//helper function to select a specific party actor or loot actor if multiple exist.
async function ActorDialog(type) {
  const content = new StringField({
    choices: game.actors.reduce((acc,e)=>{
      if(e.type !== type) return acc;
      acc[e.id] = e.name;
      return acc;
    },{}),
    label: "Please Select an Actor:",
    required: true
  }).toFormGroup({classes:["stacked"]}, {name: "actor"}).outerHTML;
  const myact = await DialogV2.prompt({
    window: {title: `Select ${type} actor`},
    content,
    ok: {
      callback: (event,button) => new FormDataExtended(button.form).object.actor
    },
    rejectClose: false,
    position: { width: 375 }
  });
  return game.actors.get(myact);
}
// creates the actual item data so the actual item can be created on the actor of choice.
async function createSpellScrollWand(itemUuid, spellUuid, level, name, mystified) {
  const spell = (await fromUuid(spellUuid))?.toObject();
  if (!spell) return null;
  if (level === false) level = spell.system.level.value;
  const item = await fromUuid(itemUuid);
  const itemData = item?.toObject();
  if (!itemData) return null;
  spell.system.location.heightenedLevel = level;
  itemData.name = name;
  itemData.system.spell = spell;
  itemData.system.description.value = `@UUID[${spellUuid}]{${spell.name}}` + itemData.system.description.value;
  itemData.system.traits.rarity = spell.system.traits.rarity;
  itemData.system.quantity = picks.quantity;
  itemData.system.traits.value = [...new Set(itemData.system.traits.value.concat(spell.system.traits.traditions).concat(spell.system.traits.value))];
  if(mystified) itemData.system.identification.status = "unidentified";
  return itemData;
}

// declaring constants.
const {StringField, NumberField, BooleanField} = foundry.data.fields;
const {DialogV2} = foundry.applications.api;
const scrollUuids = createUuids(["RjuupS9xyXDLgyIr","Y7UD64foDbDMV9sx","ZmefGBXGJF3CFDbn","QSQZJ5BC3DeHv153","tjLvRWklAylFhBHQ","4sGIy77COooxhQuC","fomEZZ4MxVVK3uVu","iPki3yuoucnj7bIt","cFHomF3tty8Wi1e5","o1XIHJ4MJyroAHfF"]);
const wandUuids = createUuids(["UJWiN0K3jqVjxvKk","vJZ49cgi8szuQXAD","wrDmWkGxmwzYtfiA","Sn7v9SsbEDMUIwrO","5BF7zMnrPYzyigCs","kiXh4SUWKr166ZeM","nmXPj9zuMRQBNT60","Qs8RgNH6thRPv2jt","Fgv722039TVM5JTc"]);
const specWandsUUIDs = {
  cont: createUuids(["a60NH7OztaEaGlU8","5V9bgqgQY1CHLd40","R88HWv9rw1VNMRer","bCsdAkffuk29ssUg","tDEi3zLVpxwA74qz","35rLqxDWgiDIkL8e","H1XGrl6Z0bzXN2oi","KMqHzKfpPq5H8GOo"]),
  reach: createUuids(["cyw2OgL4XJ9HOu0b","rmbvBjcDMDAZLJ7v","AzLEUTp4RHYAoXIe","XgKwydoro5eaIWC8","dPwRgQKEFLLDF2iB","pCr0zPdJoXZW3I6y","qeLAYEwUXNbri5eB","eFGpWmM8ehW9mkI4","sa9UGUMWYiZkTPjA"]),
  wide: createUuids(["Zw3BKaJYxxxzNZ0f","qmWlvoIlJRJ6pAeG","TJaumkbZy11sIAgR","zYRzgETeR1Hs1ti1","TGxZ3acyWjjTvfU9","JDQ4jqp6O8SurQGe","kNfdGNIGzF0fW7aq","20nQTcGzpUv8jJ6R","t5978mZ6CqfUDCP6"]),
  leger: createUuids(["z2QXO8vl0VsXaI1E","zaJ4HSNa6kMozYvM","XPqKEI246hsr9R6P","4hsPZ6rBpLKOlDjm","pdsepgrBRgdZ4DWm","dn53uqBi6MXg2gIM","AYIel6a1nARjqygh","34D6lFZ2gpZiyUU6","qoNaajuoAnKRrFyb"])};
const consumableList = {
  scroll: "PF2E.Item.Consumable.Category.scroll",
  wand: "PF2E.Item.Consumable.Category.wand",
  cont: "Wand of Continuation",
  reach: "Wand of Reaching",
  wide: "Wand of Widening",
  leger: "Wand of Legerdemain"
};
const raritiesList = CONFIG.PF2E.rarityTraits;
delete raritiesList.unique;
raritiesList.any = "Any";
const traditionsList = CONFIG.PF2E.magicTraditions;
traditionsList.random = "Random";
const outputList = {
  party: "Party Actor",
  loot: "Existing Loot Actor",
  generate: "Generated Loot Actor",
  msg: "Message"
};

// creating the fields the creation dialog will use.
const consumableField = new StringField({
  choices: consumableList,
  label: "PF2E.TraitConsumable",
  required: true
}).toFormGroup({localize: true},{localize: true, name: "consumable", autofocus:true}).outerHTML;
const spellRankField = new NumberField({
  choices:[],
  label: "PF2E.SpellLevelLabel",
  required: true
}).toFormGroup({localize: true},{name: "rank"}).outerHTML;
const traditionField = new StringField({
  label: "PF2E.MagicTraditionLabel",
  choices: traditionsList,
  required: true
}).toFormGroup({localize: true},{localize: true, name: "tradition"}).outerHTML;
const quantityField = new NumberField({
  label: "PF2E.QuantityLabel",
  required: true
}).toFormGroup({localize: true},{name: "quantity", value:1}).outerHTML;
const rarityField = new StringField({
  label: "PF2E.Rarity",
  choices: raritiesList,
  required: true
}).toFormGroup({localize: true},{localize: true, name: "rarity"}).outerHTML;
const outPutField = new StringField({
  label: "Output Type:",
  choices: outputList,
  required: true
}).toFormGroup({},{name: "outtype"}).outerHTML;
const mystifyField = new BooleanField({
  label: "PF2E.ItemMystified",
}).toFormGroup({localize: true, rootId: "scroll-wand-mystify-check"},{value: true, name: "mystify"}).outerHTML;
const dialogContent = consumableField+spellRankField+traditionField+quantityField+rarityField+outPutField+mystifyField;

const picks = await creationDialog(dialogContent);
if(!picks) return;

const data = await Promise.all(Array.from({length: picks.quantity}, () => getItem(picks.rank, picks.tradition, picks.rarity, picks.consumable)));
if(data.filter(e=>!!e).length === 0) return ui.notifications.warn("No Spells with these properties");
if(picks.outtype === "msg") {
  const content = data.filter(e=>!!e).reduce((acc,e)=>acc + `<p>@UUID[${e.uuid}]</p>`, "");
  if(!content) return;
  const item = game.i18n.format(consumableList[picks.consumable]).toLowerCase();
  await ChatMessage.create({flavor: `<strong>Random ${item}</strong><br>`, content, speaker: {alias:'GM'}, whisper:[game.user.id]});
  ui.notifications.info("Check chat message. Hold alt when dragging and dropping to mystify items");
  return;  
}
let a;
if (picks.outtype === "generate") {
  a = game.actors.getName("Generated Loot");
  if (!a) a = await Actor.create({ name: "Generated Loot", type: "loot", img: "icons/containers/chest/chest-reinforced-steel-red.webp" });
}
else {
  if (game.actors.filter(p => p.type === picks.outtype).length > 1) {
    a = await ActorDialog(picks.outtype);
  }
  else {
    a = game.actors.find(p => p.type === picks.outtype);
  }
}
if (!a) return;

const toCreate = [];
for (const s of data.filter(e=>!!e)) {
  toCreate.push(await createSpellScrollWand(s.suuid, s.uuid, s.level, s.name, picks.mystify));
}
const created = await a.createEmbeddedDocuments("Item", toCreate);
a.sheet.render(true);