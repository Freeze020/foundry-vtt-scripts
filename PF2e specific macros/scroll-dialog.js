function createSection(html){
  const scrolls = actor.itemTypes.consumable.filter(i => i.category === "scroll");
  let {scrollLevel} = new FormDataExtended(html[0].querySelector("form")).object;
  const scrollOptions = scrolls.filter(i => i.system.level.value === scrollLevel).reduce((acc,{id,name})=> acc +=`<option value="${id}">${name}</option>`,"");
  const div = `<label> Scroll Name:</label>
      <div class="form-fields">
          <select name="scrollId">${scrollOptions}</select>
      </div>`;
  const container = html[0].querySelector("div.scroll-name");
  container.innerHTML = div;
}

function onRender(html){
 createSection(html);
 html[0].querySelector(".scroll-level select").addEventListener("change", () => createSection(html));
}

const scrollLevels = Array.from({length: 10}, (_,i)=> `<option value="${i*2+1}">Level ${i+1}</option>`).join("");
const content = `<form>
  <div class="form-group scroll-level">
      <label>Scroll Spell Level:</label>
      <div class="form-fields">
          <select data-dtype="Number" name="scrollLevel">${scrollLevels}</select>
      </div>
  </div>
  <div class="form-group scroll-name">
  </div>
</form>`;

const toUse = await Dialog.prompt({
  title:"Scroll Selector",
  content,
  callback: (html) => new FormDataExtended(html[0].querySelector("form")).object.scrollId,
  render: onRender,
  label: "Use Scroll"
});
if(!toUse) return ui.notifications.warn("No spell selected!");
await game.pf2e.rollItemMacro(toUse);