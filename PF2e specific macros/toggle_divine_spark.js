// change these three uuids to the ones the actor has.
const effectUuidArray = [
  "Compendium.pf2e.classfeatures.Item.9mYcuun58wYNL1Ci", // Scar of the Survivor
  "Compendium.pf2e.classfeatures.Item.o8Q7wWx2oKvKMi1s", // Gleaming Blade
  "Compendium.pf2e.classfeatures.Item.iecFmUwSrytQNwoE"  // Skybearer's Belt
];
// make an array of items.
const effectsArray = await Promise.all(effectUuidArray.map(e=>fromUuid(e)));

// create an array with choices where the slug of the item will be used in later steps.
const choices = effectsArray.reduce((acc,{slug,name})=>{
  if(actor.rollOptions.all[`divine-spark:${slug}`]) return acc;
  acc[slug] = name;
  return acc;
},{});

// creating the dialog's html
const content = new foundry.data.fields.StringField({
  label: "Divine Spark",
  choices,
  required: true
}).toFormGroup({},{name: "slug"}).outerHTML;

// a simple prompt that returns the item slug of the chosen 
const slug = await foundry.applications.api.DialogV2.prompt({
  content,
  ok: {
    callback: (_event, button)=>new FormDataExtended(button.form).object.slug
  }
});
// get the item on the actor and use that for the toggle.
const item = actor.items.find(e=>e.slug === slug);
actor.toggleRollOption("all", "divine-spark",item.id ,true, slug);