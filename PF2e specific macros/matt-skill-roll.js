// set up MATT argsin Run Macro.
// for just a DC and triggering the dialog: 16
// for setting a DC and skill and rolling open: 20 Athletics
// for setting a DC and skill and rolling secret: 25 Society secret
const dc = args[0];
const presetSkill = args[1].slugify();
const presetSecret = args[2] === "secret";
let rollData;
if(!(presetSkill in actor.skills) && presetSkill !== "perception"){
  const options = HandlebarsHelpers.selectOptions({perception: actor.perception}, {hash: {blank: "---------------",labelAttr: "label", localize: true}}) +
                  HandlebarsHelpers.selectOptions(actor.skills, {hash: {blank: "---------------",labelAttr: "label", localize: true}});
  
  const content = `<form>
    <div class="form-group">
      <label>Skill to Roll:</label>
      <div class="form-fields">
        <select name="skill">${options}</select>
      </div>
    </div>
    <div class="form-group">
      <label>Secret Roll? </label>
      <div class="form-fields">
        <input type="checkbox" name="secret">
      </div>
    </div>
  </form>`;
  
  rollData = await Dialog.prompt({
    title: "Choose the skill",
    content,
    callback: ([html]) => new FormDataExtended(html.querySelector("form")).object
  });
  if(rollData.skill === "") return;
}
else rollData = {secret: presetSecret, skill: presetSkill};
const secret = rollData.secret ? "secret" : "";
if(rollData.skill === "perception") return actor.perception.roll({traits:[secret], dc});
return actor.skills[rollData.skill].roll({traits:[secret], dc});