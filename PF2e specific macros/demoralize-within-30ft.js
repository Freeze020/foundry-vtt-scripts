// the actual macro here
const near = [];
const visibleEnemies = canvas.tokens.placeables.filter(e => e.visible && e.actor.alliance === "opposition");
for(const enemy of visibleEnemies){
  const distance = token.distanceTo(enemy);
  if (distance <= 30) near.push(enemy);
}
// set targets by code stores current target(s) for later.
const oldTargets = game.user.targets.ids;
game.user.updateTokenTargets(near.map(e => e.id));
game.user.broadcastActivity({targets: game.user.targets.ids});
// roll the demoralize
for(const target of game.user.targets) {
  await game.pf2e.actions.get("demoralize").use({target: target.actor});
}
// restore previous targets if any.
game.user.updateTokenTargets(oldTargets);
game.user.broadcastActivity({targets: game.user.targets.ids});