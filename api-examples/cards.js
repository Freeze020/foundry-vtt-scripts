/**
 * Decks, Hands, Piles and cards and how to move the cards between them.
 */

/**
 * Creating a new deck from code.
 */
const toCreate = {
    type: "deck",
    name: "Name of Deck",
    description: "An empty deck with no cards :("
}
const deck = await Cards.create(toCreate);

/**
 * Creating a new hand from code for Bob's player.
 */
const toCreate = {
    type: "hand",
    name: "Bob's Hand",
    ownership: {
        default: CONST.DOCUMENT_OWNERSHIP_LEVELS.NONE, 
        [game.users.getName("Bob's player").id]: CONST.DOCUMENT_OWNERSHIP_LEVELS.OWNER
    }
}
const hand = await Cards.create(toCreate);

/**
 * Creating a new card pile all players can see in.
 */
const toCreate = {
    type: "pile",
    name: "Open Table pile",
    ownership: {
        default: CONST.DOCUMENT_OWNERSHIP_LEVELS.OWNER, 
    }
}
const pile = await Cards.create(toCreate);

/**
 * Creating actual cards on a new Deck. Assuming you have all your card images in one folder.
 */
const fileFolderName = "assets/cards/tarot-deck/";  //change to a folder you have your image files in, HAS to be in your data folder somewhere.
const cardBackPath = "assets/cards/tarot-deck/back/card-back.webp" // ditto
const fileList = await FilePicker.browse("data", fileFolderName);
const imageFiles = fileList.files.filter(f => ImageHelper.hasImageExtension(f));
const cardsToCreate = imageFiles.map(img => ({
    name: AudioHelper.getDefaultSoundName(img), // looks weird, but works great, changes - or _ for spaces and chops off the folder and extension.
    back: {img: cardBackPath},
    faces: [{img, name: AudioHelper.getDefaultSoundName(img)}],
    height: 3,
    width: 2,
    type: "base"
}));
const deckToCreate = {
    type: "deck",
    name: "Tarot Deck",
    description: "A mystical deck of Tarot cards"
}
const deck = await Cards.create(deckToCreate);
await deck.createEmbeddedDocuments("Card", cardsToCreate);


/**
 * Dealing random cards from the deck to a hand and/or pile face down.
 */
const deck = game.cards.getName("Tarot Deck");
const to = [ game.cards.getName("Bob's Hand"), game.cards.getName("Open Table Pile") ];
const number = 2;
await deck.deal(to, number,{how: CONST.CARD_DRAW_MODES.RANDOM}); // each hand and pile in the to array gets two cards.

/**
 * Recall all cards back to the deck.
 */
const deck = game.cards.getName("Tarot Deck");
await deck.recall();

 /**
  * Dealing random cards from the deck face up to a hand.
  */
const deck = game.cards.getName("Tarot Deck");
const to = [ game.cards.getName("Bob's Hand") ];
const number = 2;
await deck.deal(to, number,{how: CONST.CARD_DRAW_MODES.RANDOM, updateData:{face: 0}}); // in computer speak the first of an array (see faces array in the card creation) is always 0.
                                                                                       // that is why we tell it do face: 0

/**
 * Passing cards from a hand to a pile or hand (the first card of the hand to the pile face down in this example).
 */
const hand = game.cards.getName("Bob's Hand");
const pile = game.cards.getName("Open Table Pile");
const toPass = [hand.cards.getName("The Queen").id] // we need to give the function an array of card ids.
await hand.pass(pile, toPass,{updateData: {face: null}}); // if face is null it displays the card back.

/**
 * Passing all cards from a hand to a pile (now we just leave them face up);
 */
 const hand = game.cards.getName("Bob's Hand");
 const pile = game.cards.getName("Open Table Pile");
 const toPass = hand.cards.map(c => c.id); // this returns an array of all card ids in the hand of Bob.
 await hand.pass(pile, toPass);
 