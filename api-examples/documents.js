/**
 * get different documents by ID from their respecitive library, not an exhaustive list.
 */
const myActor = game.actors.get("<<id of actor>>");
const myScene = game.scenes.get("<<id of scene>>");
const myJournal = game.journal.get("<<id of journal>>");
const myPlaylist = game.playlists.get("<<id of playlist>>");

/**
 * get different documents by Name from their respective library, not an exhaustive list.
 */
 const myActor = game.actors.getName("Bob");
 const myScene = game.scenes.getName("Bob's home");
 const myJournal = game.journal.getName("Bob's diary>");
 const myPlaylist = game.playlists.getName("Bob's theme music");

/**
 * get an array of actors by filtering for actors owned by players. The filters below can be for any property the document has.
 */
const playerActors = game.actors.filter(a => a.hasPlayerOwner);

/**
 * get an array of actors not owned by players
 */
const otherActors = game.actors.filter(a => !a.hasPlayerOwner);
