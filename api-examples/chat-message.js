/**
 * create a chat message
 */
ChatMessage.create({ content: 'Hello World!' });

/**
 * Send chat message under an alias
 */
ChatMessage.create({ 
    content: "Blah blah blah", 
    speaker: { alias: "Not Bob, definitely not Bob" } 
});

/**
 * Send chat message emote as a given actor, WITH chat bubble
 */ 
let actr = game.actors.getName('Bob');
let spkr = ChatMessage.getSpeaker({ actor: actr });
ChatMessage.create({
  speaker: spkr,
  content: "...turns his head toward Steve",
  type: CONST.CHAT_MESSAGE_TYPES.EMOTE
},
{ chatBubble: true });

/**
 * Send a whisper to a user or users.
 */
ChatMessage.create({
  content: `Hello`,
  whisper: ["<<id of user>>", game.users.getName("Fred")]  // can take ids and/or User documents (even mixed).
});
// Whisper player by user name.
ChatMessage.create({
  content: `Hello`,
  whisper: ChatMessage.getWhisperRecipients('Janet'),
});
//whisper player by character name of owned actor.
ChatMessage.create({
    content: `Hello`,
    whisper: ChatMessage.getWhisperRecipients('Bob'),
  });
// Whisper GM(s)
ChatMessage.create({
  content: `Hello`,
  whisper: ChatMessage.getWhisperRecipients('GM'),
});
// Whisper to all players
ChatMessage.create({
    content: `Hello`,
    whisper: ChatMessage.getWhisperRecipients('players'),
  });

/**
 * Add a sound to a chat message
 */
ChatMessage.create({
    content: "Hello",
    sound: "assets/sfx/hello.mp3"
});

/**
 * List of message types
 */
CONST.CHAT_MESSAGE_TYPES.OOC
CONST.CHAT_MESSAGE_TYPES.IC
CONST.CHAT_MESSAGE_TYPES.EMOTE
// .WHISPER is selected automatically when adding whisper to the object (as above), 
// .ROLL is only used when you add rolls: [array of rolls] to the chatData
