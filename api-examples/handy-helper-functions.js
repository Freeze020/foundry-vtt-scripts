/**
 * Assorted list of FoundryVTT helper functions to make your life easier.
 * All of these and more are found in the commons.js of Foundry VTT. It is a treasure trove of methods to make life easier!
 */

/**
 * duplicate is a very useful method to safely duplicate an object or array for modification especially when you need to update a document's key that has an array as value.
 */
const changes = actor.effects.find(e => e.label === "my effect").changes;
let dupChanges = foundry.utils.duplicate(changes);
// do something with the duplicate then update the effect with that altered array.


/**
 * hasProperty is an effective way to see if a key exists in an object, again useful in (pre)update hooks.
 */
// checking whether a token moved to the left or right (x position has to be updated)
Hooks.on("preUpdateToken", (_, change) => {
    const hasProp = foundry.utils.hasProperty(change, "x");
    if(!hasProp) return console.log("token did not move horizontally");
    return console.log("token did move horizontally");
});


/**
 * getProperty can be used to retrieve a property from an object.
 */
const src = foundry.utils.getProperty(token.document, "texture.src"); // returns the path of the texture source of the token

/**
 * setProperty can be effectively used in preUpdate hooks as above, to add onto the update object.
 */
 Hooks.on("preUpdateToken", (_, change) => {
    const hasProp = foundry.utils.hasProperty(change, "x");
    if(!hasProp) return console.log("token did not move horizontally");
    return foundry.utils.setProperty(change, "texture.src", "assets/tokens/horizontal-hero.webp");
});


/**
 * mergeObject is great for you have an object and you want to merge it with a different object. Order of operation is important.
 */
const settings = game.settings.get("core", "defaultToken"); // gets a small object containing the default token settings, like displayName etc.
const updates = canvas.scene.tokens.map(token => {
    const updateObject = {_id: token.id};
    foundry.utils.mergeObject(updateObject, settings); // the first argument gets the second argument added to it, and if a key in the first argument exists in the second it gets overwritten.
});
await canvas.scene.updateEmbeddedDocuments("Token", updates);

/**
 * Foundry utilizes 16digit random ids, how? well with randomID
 */
const rndID = foundry.utils.randomID(); // '8lijxLUiYpiAlGuj' for example
const shortRandomID = foundry.utils.randomID(8); // 'rjDkKVTc' fore example


/**
 * Custom Array function from Foundry VTT: fromRange
 */
const range = Array.fromRange(6); // [0,1,2,3,4,5]

/**
 * Custom Array function from Foundry VTT: partition
 */
const tokens = canvas.tokens.placeables;
const [npcTokens, pcTokens] = tokens.partition(t => t.actor.type === "character"); // returns a 2D array [[failedTest], [passedTest]]

/**
 * Custom Number function from Foundry VTT: between
 * sometimes it is useful to know if a value of X lies between A and B.
 */
const value = Math.ceil(Math.random() * 10); // value between 1 and 10
const isBetween =  value.between(3,8);
if(!isBetween) console.log("the random number is higher than 8 or lower than 3");

/**
 * Custom Number function from Foundry VTT: ordinalString
 * turns positive integers into ordinal numbers, ie 1 -> 1st or 93 -> 93rd or 1169 -> 1169th
 */
const myArray = Array.fromRange(6).map(n => (n+1).ordinalString()); // ["1st","2nd", "3rd", "4th", "5th", "6th"]
