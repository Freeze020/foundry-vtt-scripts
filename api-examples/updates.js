/**
 * single document update
 */
const tokenDoc = canvas.scene.tokens.getName("Bob's token");
await tokenDoc.update({"texture.src": "assets/pcs/tokens/Bob-2.png"});

/**
 * batch document update
 */
const updates = game.actors.map(a => ({_id: a.id, name: a.name + "!"})); // adds a ! to all actor names
await Actor.updateDocuments(updates);

/**
 * batch embedded document update
 */
const updates = canvas.drawings.controlled.map(d => ({_id: d.id, fillColor: "#FF0000"}));  // turns all drawings with a fill to a pure red fill color.
await canvas.scene.updateEmbeddedDocuments("Drawing", updates);

/**
 * update all placeables (and possible filters)
 */
await canvas.lighting.updateAll({hidden: true}); // turns off ALL lights on the canvas.
await canvas.tokens.updateAll(t => ({x: t.document.x + 100})); // moves all documents 100px from their original spot
await canvas.tiles.updateAll(t => ({x: t.document.x - 100}), t => t.document.texture.src === "assets/map/light-sources/chandelier.png" ); // moves all tiles with the texture chandelier, 100px to the left.
