/**
 * get the first controlled / selected token document
 */
const myToken = canvas.tokens.controlled[0].document;

/**
 * or get a token document by name
 */
const myToken = canvas.scene.tokens.getName("Name of Token");

/**
 * or get a token document by id
 */
const myToken = canvas.scene.tokens.get("<<id of token>>"); // id is a 15 character string given when the document is created.

/**
 * or get a token document by target
 */
const myToken = game.user.targets.first().document;

/**
 * log your token to the console
 */
console.log(myToken);

/**
 * creating a token document from an actor's prototype token using the options object to make changes to the token to be.
 */
let tk = await game.actors.getName("Name of Actor").getTokenDocument({x: 500, y: 500, name: "perhaps a different name from the actor"});
const created = await canvas.scene.createEmbeddedDocuments("Token", [tk.toObject()]);

/**
 * deleting a token document from the canvas.
 */
await myToken.delete();

/**
 * deleting all controlled token documents from the canvas
 */
const toDelete = canvas.tokens.controlled.map(t => t.id);
const deleted = await canvas.scene.deleteEmbeddedDocuments("Token", toDelete);

/**
 * control all tokens on the canvas
 */
canvas.tokens.controlAll();

/**
 * control some tokens on the canvas by some property they have, say disposition hostile
 */
const hostiles = canvas.scene.tokens.filter(t => t.disposition === CONST.TOKEN_DISPOSITIONS.HOSTILE);
hostiles.forEach(t=> {
    t.object.control({releaseOthers: false})
});

/**
 * distance between two tokens
 */
 const distance = canvas.grid.measureDistance(token1, token2);