/*
 * Create and share an image popout
 */
if(!token) return;
const pop = new ImagePopout(token.document.texture.src, { title: `Image of ${token.document.name}`, caption: token.document.name });
pop.render(true); // Display for self
pop.shareImage(); // Display to all other players