/**
 * Examples taken from from VanceCole's repository
 * Basic structure of the Dialog (from the API, with added optional object)
 */
const d = new Dialog({
    title: "Test Dialog",
    content: "<p>You must choose either Option 1, or Option 2</p>",
    buttons: {
        one: {
            icon: '<i class="fas fa-check"></i>',
            label: "Option One",
            callback: () => console.log("Chose One")
        },
        two: {
            icon: '<i class="fas fa-times"></i>',
            label: "Option Two",
            callback: () => console.log("Chose Two")
        }
    },
    default: "two",
    render: html => console.log("Register interactivity in the rendered dialog"),
    close: html => console.log("This always is logged no matter which option is chosen")
}, {
    // optional object nothing here is needed to make a basic dialog work.
    id: "my-unique-id-for-this-dialog", // name as you want just make it unique and no spaces! needed for CSS selecting if you add a <style> </style> to the content.
    classes: ["dialog", "stuff", "etc"], // as with id.
    width: 300, // default "auto" and cant go narrower than 200
    height: 600, // default "auto"
    resizable: true, // default false
    top: 100, // location on screen where the top of the dialog sits, default is in the center of the screen
    left: 100, // location on screen where the left of the dialog sits, default is in the center of the screen
});
d.render(true);


/**
 * Premade simple dialog with a return value.
 * after user clicks button, confirmation will be boolean for yes/no
 */
let confirmation = await Dialog.confirm({
    title: 'Example Confirm',
    content: `<p>Are you sure?</p>`,
});


/**
 * Very simple dialog to request a user input returns the user input as a string.
 */
let myValue = await Dialog.prompt({
    content: `<input type="text">`,
    callback: (html) => html.find('input').val()
});


/**
 * Example dialog that requests user input, then uses the value
 */
let data = await Dialog.wait({
    title: 'Example',
    content: `
        <form>
            <div class="form-group">
                <label for="exampleInput">Example Input</label>
                <div class="form-fields">
                    <input type="text" name="exampleInput" placeholder="Enter Value">
                </div>
            </div>
            <div class="form-group">
                <label for="exampleSelect">Example Select</label>
                <div class="form-fields">
                    <select name="exampleSelect">
                        <option value="option1">Option One</option>
                        <option value="option2">Option Two</option>
                        <option value="option3">Option Three</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleColor">Example Color</label>
                <div class="form-fields">
                    <input class="color" type="text" name="exampleColor" value="#ff6400">
                    <input type="color" value="#ff6400" data-edit="exampleColor">
                </div>
            </div>
            <div class="form-group">
                <textarea name="exampleText" placeholder="Enter Text"></textarea>
            </div>
      </form>
    `,
    buttons: {
        no: {
            icon: '<i class="fas fa-times"></i>',
            label: 'Cancel'
        },
        yes: {
            icon: '<i class="fas fa-check"></i>',
            label: 'Yes',
            callback: (html) => new FormDataExtended(html[0].querySelector("form")).object
        },
    },
    default: 'yes',
    close: () => {
        console.log('Example Dialog Closed');
    }
});
console.log(data) // data is an object containing keys based on the name attributes of the inputs and selects.


/**
 * Dialog that re-renders (stays open) when button is clicked
 * rather than closing
 */
let myContent = function (val) {
    return `
    <div class="main">
        <div class="headline">
            Attack information:
        </div>
        <div class="plain-text">
            <div class="actor" id="currentActor">${val}</div>
        </div>
    </div>
    `;
}

let myDialog = new Dialog({
    title: `Example Dialog`,
    content: myContent('Default Value'),
    buttons: {
        update: {
            label: "Update",
            callback: () => {
                myValue = `Example random value: ${Math.random() * 100}`;
                myDialog.data.content = myContent(myValue);
                myDialog.render(true);
            }
        }
    },
},
    {
        id: 'test'
    }
).render(true);
