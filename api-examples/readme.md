### Reference for common used functions

#### What this isn't

This isn't a resource to learn JavaScript. Basic JavaScript concepts like Strings, Integers, Objects and Arrays  
should already be familiar or a lot will not make sense.  
This also isn't me telling you that this is how things should be done, only how things can be done.  

#### What this is

This is a collection of short examples, displaying how common Foundry VTT functions are used.  
Inspired by and borrowed from work by [VanceCole](https://github.com/VanceCole/macros) whose hard work is in places  
outdated due to API changes over the years.
