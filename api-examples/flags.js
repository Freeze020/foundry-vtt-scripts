/**
 * Fun with Flags...
 * 
 * Document#setFlag(scope, key, value)
 * Docment#getFlag(scope, key) returns value
 * Document#unsetFlag(scope, key)
 * 
 * Valid scopes are: id of an active module (eg. "dice3d"), system id (eg. "dnd5e"), "core" (do not use unless 1000% sure), 
 * and "world" (this is 99% of the time what you use unless interacting with a module's flag or system flag)
 * 
 * Valid keys are: "my-flag" or "myFlag" or "previous-value" anything really goes, just dont use white spaces so no: "my flag"
 */


/** 
 * Create a world flag on a scene
 */ 
await canvas.scene.setFlag('world', 'myFlag', 'myValue');

/**
 * Retrieve a world flag on a scene
 */
let x = canvas.scene.getFlag('world', 'myFlag');

/**
 * Unset a world flag on a scene
 */
await canvas.scene.unsetFlag('world', 'myFlag');

/**
 * Flags can be arrays, objects, numbers etc as well
 * Anything that can be stringified
 */
let data = {
  myString: 'hello',
  myNum: 42,
  myBool: true,
  myArray: ['a','b','c']
}
await canvas.scene.setFlag('world', 'myFlag', data);

/**
 * Special: removing a nested property
 */
await canvas.scene.unsetFlag('world', 'myFlag.myBool'); // would remove the myBool property from above only.
