//////////////////////////////// style options /////////////////////////////////////////////
const arrowLength = 30; // how long is the arrow in px.
const arrowWidth = 15;  // how wide is the arrow in px.
const width = 10;       // width of the line.
const alpha = 1;      // how visible is the line. gives slight artefacts when lower than 1.
///////////////////////////////////////////////////////////////////////////////////////////
let start = 0;
let startPoint = token.center;
let request;
// listening for the clicks
drawLine();

canvas.app.stage.on("click", function animateLine(){
    canvas.app.stage.off("click", animateLine)
    cancelAnimationFrame(request);
    canvas.app.stage.removeChild(canvas.app.stage.children.find(c => c.name === "tempLine"));
});
//////////////////////////////////////
function makeLine(line){
    const fill = game.user.color;
    line.lineStyle({
        width,
        alpha, 
        color: fill.replace("#", "0x"),
        join: "round",
        cap: "round"
    });
    const mousePos = game.version < 11 ? canvas.app.renderer.plugins.interaction.mouse.getLocalPosition(canvas.app.stage) : canvas.mousePosition;
    line.moveTo(startPoint.x, startPoint.y);
    let dx = mousePos.x - startPoint.x;
    let dy = mousePos.y - startPoint.y;
    let l = Math.sqrt(dx * dx + dy * dy);
    if (l !== 0) {
        let nx = dx/l; let ny = dy/l;  
        let ex = startPoint.x + nx * l;
        let ey = startPoint.y + ny * l;
        let sx = startPoint.x + nx * (l - arrowLength);
        let sy = startPoint.y + ny * (l - arrowLength);
        line.lineTo(ex, ey);
        // line.moveTo(ex, ey);
        line.lineTo(sx - ny * arrowWidth, sy + nx * arrowWidth);
        line.moveTo(ex, ey);
        line.lineTo(sx + ny * arrowWidth, sy - nx * arrowWidth);
    }
    line.name = "tempLine";
    line.alpha = 1;
    return line;
}

function step(timestamp) {
    let marker = canvas.app.stage.children.find(c => c.name == "tempLine");
    canvas.app.stage.removeChild(marker);
    if (start === 0) start = timestamp;
    const elapsed = timestamp - start;
    const line = makeLine(new PIXI.Graphics());
    canvas.app.stage.addChild(line);
    if (elapsed < 1000) request = requestAnimationFrame(step);
    else { 
        start = 0;
        //cancelAnimationFrame(request);  // cancels the animation after 1 second
        request = requestAnimationFrame(step); // continues the animation indefinetly. choose either one.
    }
}

function drawLine(){
    const line = makeLine(new PIXI.Graphics());
    canvas.app.stage.addChild(line);
    requestAnimationFrame(step);
}
