// v12 script, wont work in v11 or lower!
// a bit of an icky way to remove the grey background on effect icons on a token. Systems/modules that override this same method will either behave weird or may seize to work. Use at own risk!
// add this script to a world-script or make a small module.
Hooks.once("init",()=>{
    Token.prototype._drawEffects = async function() {
        this.effects.renderable = false;

        // Clear Effects Container
        this.effects.removeChildren().forEach(c => c.destroy());
        this.effects.bg = this.effects.addChild(new PIXI.Graphics());
        this.effects.bg.zIndex = -1;
        this.effects.overlay = null;
        this.effects.bg.alpha = 0; // this line is the only change to the actual method. everything else is as it was.

        // Categorize effects
        const activeEffects = this.actor?.temporaryEffects || [];
        const overlayEffect = activeEffects.findLast(e => e.img && e.getFlag("core", "overlay"));

        // Draw effects
        const promises = [];
        for ( const [i, effect] of activeEffects.entries() ) {
        if ( !effect.img ) continue;
        const promise = effect === overlayEffect
            ? this._drawOverlay(effect.img, effect.tint)
            : this._drawEffect(effect.img, effect.tint);
        promises.push(promise.then(e => {
            if ( e ) e.zIndex = i;
        }));
        }
        await Promise.allSettled(promises);

        this.effects.sortChildren();
        this.effects.renderable = true;
        this.renderFlags.set({refreshEffects: true});
    }
});