// pan to an NPC on the canvas or share the image(click) of an NPC (shift click)
// v12 macro.

const options = canvas.scene.tokens.filter(t => !t.actor.hasPlayerOwner).reduce((acc, t) =>
  acc += `<img class="token-image" src="${t.texture.src}" data-id="${t.id}" title="${t.name}">`,
  ``);

const content = `<style>
  #dialog-show-token-art .token-list {
    padding: 6%;
    display: grid;
    gap: 3em;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    max-height: 430px;
    overflow-y: scroll;
  }
  #dialog-show-token-art .token-list .token-image {
    border:0px;
    transition: all 0.3s ease-out;
  }
  #dialog-show-token-art .token-list .token-image:hover {
    transform: scale(1.75);
    filter: drop-shadow(0px 0px 5px #840);
  }
</style>
<form>
  <div class="token-list">
    ${options}
  </div>
  <p class="hint">Left click to pan all connected users to token, SHIFT+Lclick to show token Image Popout.</p>
</form>`;


const dialog = new foundry.applications.api.DialogV2({
  window: { title: "Show Token Art", icon: "fa-solid fa-user" },
  content,
  buttons: [
    {
      label: "Done",
      icon: "fa-solid fa-check",
      action: "ok"
    }
  ],
  position: {
    width: 600,
    left: 0
  },
  id: "dialog-show-token-art",
  default: "yes",
});

dialog.addEventListener("render", () => {
  const html = dialog.element;
  html.querySelectorAll(".token-image").forEach(e => {
    e.addEventListener("click", function (event) {
      if (event.shiftKey) {
        const popout = new ImagePopout(event.target.src);
        popout.shareImage();
      }
      else {
        const t = canvas.tokens.get(event.target.dataset.id);
        canvas.ping(t.center, {
          size: 0,
          pull: true,
          zoom: 1
        });
      }
    });
    e.addEventListener("mouseenter", function (event) {
      canvas.tokens.get(event.target.dataset.id)._onHoverIn(event, { hoverOutOthers: true });
    });
    e.addEventListener("mouseleave", function (event) {
      canvas.tokens.get(event.target.dataset.id)._onHoverOut(event);
    });
  });
});

dialog.render({ force: true });