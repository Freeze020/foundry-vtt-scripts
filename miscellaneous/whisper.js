const users = game.users.filter(user => user.active);
let contentOptions = "";

// Build checkbox list for all active players
users.forEach(user => {
  const checked = !user.isGM ? "checked" : "";
  contentOptions +=`
    <br>
    <input type="checkbox" name="checkbox-${user.id}" value="${user.name}" ${checked}>\n
    <label for="checkbox-${user.id}">${user.name}</label><br>
    <label for="whisper-message-${user.id}">Message:</label>
    <textarea id="whisper-message-${user.id}" name="message-${user.id}" rows="1" cols="50"></textarea><hr>
  `;
});

new Dialog({
    title:"Whisper",
    content:`Whisper To: ${contentOptions} <br>`,
    buttons:{
        whisper:{   
            label:"Whisper",
            callback: (html) => createMessage(html)
        }
    }
},{
    id: "whisper-dialog"
}).render(true);

async function createMessage(html) {
    for (let user of users) {
        if (html.find(`[name=checkbox-${user.id}]`)[0].checked){
            const messageText = html.find(`[name=message-${user.id}]`)[0].value;
            console.log(messageText);
            await ChatMessage.create({
                content: messageText,
                whisper: [user.id]
            });
        }
    }
}
