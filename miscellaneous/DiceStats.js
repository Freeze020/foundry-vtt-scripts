// this macro gets all the d20 rolled from the messages and spits back what is the avg roll and all rolled d20 results.

const rollMessages = game.messages.filter(m => m.rolls.some(e => e.formula?.includes("d20")));
const rollArray = rollMessages.reduce((acc, message) => {
    acc = acc.concat(message.rolls.reduce((results, roll) => {
        for (let term of roll.terms) {
            if (term.faces !== 20) continue;
            results = results.concat(term.results);
        }
        return results;
    }, []));
    return acc;
}, []);

const { critCount, failCount } = rollArray.reduce((acc, e) => {
    if (e.result === 20) acc.critCount++;
    if (e.result === 1) acc.failCount++;
    return acc;
}, { critCount: 0, failCount: 0 });

const sumRolls = rollArray.reduce((acc, e) => acc + e.result, 0);
const averageRolls = sumRolls / rollArray.length;
let allRolls = rollArray.map(e => e.result).join("-");
allRolls = allRolls.replaceAll(/(?<=\D)20(?=\D)/g, "<strong style='color: #18520b;'>20</strong>");
allRolls = allRolls.replaceAll(/(?<=\D)1(?=\D)/g, "<strong style='color:#aa0200;'>1</strong>");

await ChatMessage.create({
    flavor: 'Dice stats:',
    content: `<p>In today's session ${rollArray.length} d20's were rolled.</p>
        <p>An avgerage of ${averageRolls.toFixed(2)} was rolled.</p>
        <p>${critCount} ${critCount === 1 ? `critical success was rolled.` : `critical successes were rolled.`}</p> 
        <p>${failCount} ${failCount === 1 ? `critical fail was rolled.` : `critical fails were rolled.`}</p> 
        <p>Raw numbers:${allRolls}</p>`
});