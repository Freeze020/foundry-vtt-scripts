// Author: Freeze2689 on Discord.
// foundry ver v12+

async function MoveX(direction, k, distance) {
  distance = direction == "left" ? distance * -1 : distance;
  k = Number(k);
  if (canvas.walls.controlled.length > 1) return ui.notifications.warn("too many wallsegments selected.");
  const wallToMove = canvas.walls.controlled;
  let c = foundry.utils.duplicate(wallToMove[0].document.c);
  const joinedWall = canvas.walls.placeables.filter(w => ((w.document.c[0] === c[k] && w.document.c[1] === c[k + 1]) || (w.document.c[2] === c[k] && w.document.c[3] === c[k + 1])) && w.id !== wallToMove[0].id);
  const joinedWallUpdates = joinedWall.map(wall => {
    let coords = foundry.utils.duplicate(wall.document.c);
    if (coords[1] === c[k + 1] && coords[0] === c[k]) { coords[0] += distance; }
    if (coords[3] === c[k + 1] && coords[2] === c[k]) { coords[2] += distance; }
    return { _id: wall.id, c: coords };
  });
  c[k] += distance;
  let updates = wallToMove.map(wall => ({ _id: wall.id, c }));
  updates = updates.concat(joinedWallUpdates);
  await canvas.scene.updateEmbeddedDocuments("Wall", updates);
}

async function MoveY(direction, k, distance) {
  if (canvas.walls.controlled.length > 1) return ui.notifications.warn("too many wallsegments selected.");
  k = Number(k);
  distance = direction == "up" ? distance * -1 : distance;
  const wallToMove = canvas.walls.controlled;
  let c = foundry.utils.duplicate(wallToMove[0].document.c);
  const joinedWall = canvas.walls.placeables.filter(w => ((w.document.c[1] === c[k] && w.document.c[0] === c[k - 1]) || (w.document.c[3] === c[k] && w.document.c[2] === c[k - 1])) && w.id !== wallToMove[0].id);
  const joinedWallUpdates = joinedWall.map(wall => {
    let coords = foundry.utils.duplicate(wall.document.c);
    if (coords[0] === c[k - 1] && coords[1] === c[k]) { coords[1] += distance; }
    if (coords[2] === c[k - 1] && coords[3] === c[k]) { coords[3] += distance; }
    return { _id: wall.id, c: coords };
  });
  c[k] += distance;
  let updates = wallToMove.map(wall => ({ _id: wall.id, c }));
  updates = updates.concat(joinedWallUpdates);
  await canvas.scene.updateEmbeddedDocuments("Wall", updates);
}

const content = `<p>-normal click: 1px</p><p>-shift + click: 10px</p>
<fieldset><legend>Node 1</legend>
  <button type="button" data-direction="left" data-k="0" class="wall-mover-button"><i class="fas fa-angle-left"></i></button>
  <button type="button" data-direction="right" data-k="0" class="wall-mover-button"><i class="fas fa-angle-right"></i></button>
  <button type="button" data-direction="up" data-k="1" class="wall-mover-button"><i class="fas fa-angle-up"></i></button>
  <button type="button" data-direction="down" data-k="1" class="wall-mover-button"><i class="fas fa-angle-down"></i></button>
</fieldset>
<fieldset><legend>Node 2</legend>
  <button type="button" data-direction="left" data-k="2" class="wall-mover-button"><i class="fas fa-angle-left"></i></button>
  <button type="button" data-direction="right" data-k="2" class="wall-mover-button"><i class="fas fa-angle-right"></i></button>
  <button type="button" data-direction="up" data-k="3" class="wall-mover-button"><i class="fas fa-angle-up"></i></button>
  <button type="button" data-direction="down" data-k="3" class="wall-mover-button"><i class="fas fa-angle-down"></i></button>
</fieldset>`;

let d = new foundry.applications.api.DialogV2({
  window: { title: "Wall mover" },
  content,
  buttons: [
    {
      label: `Done`,
      action: "done"
    }
  ]
}, {
  id: "wall-mover-dialog",
  width: 200
});

d.addEventListener("render", (renderEvent) => {
  d.element.querySelectorAll(".wall-mover-button").forEach(e => e.addEventListener("click", function (event) {
    const { direction, k } = this.dataset;
    const distance = event.shiftKey ? 10 : 1;
    if (direction === "left" || direction === "right") MoveX(direction, k, distance);
    if (direction === "up" || direction === "down") MoveY(direction, k, distance);
  }));
});
d.render({ force: true });