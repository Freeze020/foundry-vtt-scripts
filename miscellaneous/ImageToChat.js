//v12+ macro
const { FilePathField, StringField, BooleanField } = foundry.data.fields;

async function imageMessage(button, mode) {
    const { url, whisper, player } = new FormDataExtended(button.form).object;
    if (url === "") {
        return;
    }
    if (mode == "chat") {
        const messageContent = `<img src="${url}" />`;
        if (!whisper) return await ChatMessage.create({ content: messageContent });

        const playerName = game.users.get(player).name;
        return await ChatMessage.create({ content: messageContent, whisper: ChatMessage.getWhisperRecipients(playerName) });
    }
    if (mode == "popout") {
        const popout = new ImagePopout(url).render(true);
        return popout.shareImage();
    }
}

let choices = game.users.filter(u => u.role < CONST.USER_ROLES.ASSISTANT).reduce((acc, e) => {
    acc[e.id] = e.name;
    return acc;
}, {});
const pickerField = new FilePathField({
    categories: ["IMAGE"],
    label: "URL:",
    hint: "If the image is from the internet do not forget to include http(s):// in the url."
}).toFormGroup({}, { name: "url" }).outerHTML;
const whisperField = new BooleanField({
    label: "Whisper to player?"
}).toFormGroup({ rootId: "image-to-chat-dialog-whisper-checkbox" }, { name: "whisper" }).outerHTML;
const userField = new StringField({
    label: "User:",
    choices,
    required: true
}).toFormGroup({}, { name: "player", disabled: true }).outerHTML;
let dialogContent = `
    <h2>Paste your image url below:</h2>
    ${pickerField}
    ${whisperField}
    ${userField}`;

const d = new foundry.applications.api.DialogV2({
    title: "Image to Chat",
    content: dialogContent,
    buttons: [
        {
            label: "To Chat!",
            action: "send",
            callback: async (event, button) => {
                await imageMessage(button, "chat");
            }
        },
        {
            label: "Pop out!",
            action: "pop",
            callback: async (_event, button) => {
                await imageMessage(button, "popout");
            }
        },
        {
            label: "Close",
            action: "foo",
            callback: () => d.close()
        }
    ],
    form: { closeOnSubmit: false },
    id: "image-to-chat-dialog",
    position: {
        left: 100,
        top: 100,
        width: 450
    }
});
d.addEventListener("render", () => {
    const html = d.element;
    html.querySelector("[name=whisper]").addEventListener("change", (event) => {
        const select = html.querySelector("[name=player]");
        select.disabled = !select.disabled;
    });
    html.querySelectorAll(".form-footer > button").forEach(e => e.style["min-width"] = "133px");
});

d.render(true);