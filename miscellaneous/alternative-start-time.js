// v12+
const { DialogV2 } = foundry.applications.api;
const { NumberField, StringField } = foundry.data.fields;

function changeContent(html, select) {
    const oldSoundField = html.querySelector("[name=songId]");
    if (oldSoundField) oldSoundField.closest(".form-group").remove();
    const oldStartField = html.querySelector("[name=startTime]");
    if (oldStartField) oldStartField.closest(".form-group").remove();
    const soundChoices = game.playlists.get(select.value).sounds.reduce((acc, { id, name }) => {
        acc[id] = name;
        return acc;
    }, {});
    const soundField = new StringField({
        choices: soundChoices,
        label: "Song/Sound",
        required: true
    }).toFormGroup({}, { name: "songId" }).outerHTML;
    const startField = new NumberField({
        label: "Start time (in s):"
    }).toFormGroup({}, { name: "startTime", value: 0, min: 0 }).outerHTML;
    html.querySelector(".dialog-content").insertAdjacentHTML("beforeend", soundField);
    html.querySelector(".dialog-content").insertAdjacentHTML("beforeend", startField);
}

function render(event, html) {
    const plSelect = html.querySelector("[name=listId]");
    changeContent(html, plSelect);
    plSelect.addEventListener("change", () => {
        changeContent(html, plSelect);
    });
}

async function startSong({ startTime = 0, listId = "", songId = "" } = {}) {
    let plist = game.playlists.get(listId);
    let song = plist.sounds.find(s => s.id === songId);
    await plist.updateEmbeddedDocuments("PlaylistSound", [{ _id: song.id, pausedTime: startTime, playing: true }]);
}

const playListChoices = game.playlists.reduce((acc, { id, name }) => {
    acc[id] = name;
    return acc;
}, {});
const playListField = new StringField({
    label: "Playlist:",
    choices: playListChoices,
    required: true
}).toFormGroup({}, { name: "listId" }).outerHTML;


await startSong(await DialogV2.prompt({
    window: { title: "Alternative Start Time" },
    content: playListField,
    ok: {
        callback: (_event, button) => new FormDataExtended(button.form).object
    },
    render: render,
    position: { width: 450 }
}));