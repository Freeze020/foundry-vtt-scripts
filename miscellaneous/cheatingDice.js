const { DialogV2 } = foundry.applications.api;
const { NumberField, StringField } = foundry.data.fields;

const formulaField = new StringField({
    label: "Roll Formula"
}).toFormGroup({}, { name: "formula" }).outerHTML;
const resultField = new NumberField({
    label: "Desired Outcome"
}).toFormGroup({}, { name: "outcome" }).outerHTML;


await DialogV2.prompt({
    window: { title: "loaded dice dialog" },
    content: formulaField + resultField,
    ok: {
        callback: cheatingDiceRoll

    },
    position: { width: 450 }
});


async function cheatingDiceRoll(_event, button) {
    const { formula, outcome } = new FormDataExtended(button.form).object;
    if (!Roll.validate(formula)) return ui.notifications.warn("Not a valid dice formula");
    let roll = await new Roll(formula).evaluate();
    let diceDifference = outcome - roll.total;
    let modifierTotal = roll.total - roll.dice.reduce((acc, e) => acc += e.total, 0);
    const maxEyes = roll.dice.reduce((acc, e) => acc += e.faces * e.number, 0);
    const minEyes = roll.dice.reduce((acc, e) => acc += e.number, 0);
    if (outcome < minEyes + modifierTotal || outcome > maxEyes + modifierTotal) return ui.notifications.warn("Your cheated number is outside the range of possibility");
    while (diceDifference !== 0) {
        const dice = roll.terms.filter(e => e instanceof CONFIG.Dice.termTypes.DiceTerm);
        const change = diceDifference > 0 ? -1 : 1;
        for (let die of dice) {
            for (let result of die.results) {
                if ((result.result === 1 && change === 1) || (result.result === die.faces && change === -1) || diceDifference === 0) continue;
                result.result -= change;
                diceDifference += change;
            }
        }
    }
    roll._total = outcome;
    await roll.toMessage({}, { rollMode: "blindroll" });
}