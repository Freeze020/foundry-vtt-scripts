/**
 * experimental!
 *
 * this macro will create a scenes folder named after the folder your files are in.
 * it will automatically create the scenes with the name of the file as name of the scene.
 * you will have to provide a dpi of the maps, so if some are of different dpi, this will require editing later i suggest using the most common one in the dialog.
 * the path you need to enter => modules/some module/maps or worlds/myworld/battlemaps etc. as long as the last folder is indeed where the pngs or webps are located.
 */
const { DialogV2 } = foundry.applications.api;
const { NumberField, FilePathField } = foundry.data.fields;

async function createScenes(_event, button) {
  // setting the folder and the resolution
  const { folder, resolution } = new FormDataExtended(button.form).object;
  // getting the files from the designated folder...
  const battleMapsList = await FilePicker.browse("data", folder, { extensions: [".png", ".jpg", ".webp"] });
  const battleMaps = battleMapsList.files;
  // preparing data for the Scene creation.
  let folderName = folder.replace(/\//g, "-");  // removes  "/" in favor of "-"
  let gameFolder = await Folder.create({ name: folderName, type: "Scene" }); // creates the folder to dump in the scenes.
  // here we iterate over each file in the data folder.
  for (let battleMap of battleMaps) {
    const { width, height } = await loadTexture(battleMap)   // this is all to get the dimensions of the image.
    const battleMapName = foundry.audio.AudioHelper.getDefaultSoundName(battleMap);
    await Scene.create({ name: battleMapName, "background.src": battleMap, folder: gameFolder.id, navigation: false, padding: 0.05, "grid.size": resolution, width, height });
  }
}
const assetsField = `<div class="form-group">
  <label>Folder:</label>
  <div class="form-fields">
    <file-picker type="folder"></file-picker>
  </div>
  <p class="hint">Folder where the images you want to turn into Scenes are found.</p>
</div>`;
const resolutionField = new NumberField({
  label: "Resolution",
  hint: "Here you pick the pixels per square, if not all files have the same resolution, pick the most common."
}).toFormGroup({}, { name: "resolution" }).outerHTML;

await DialogV2.prompt({
  window: { title: "Scene creator" },
  content: assetsField + resolutionField,
  ok: {
    label: "GO!",
    callback: createScenes
  },
  position: { width: 500 }
});
