const {sceneX, sceneY, sceneHeight, sceneWidth} = canvas.scene.dimensions;
const container = new PIXI.Container({width: sceneWidth, height: sceneHeight});
const sceneSprite = PIXI.Sprite.from(canvas.scene.background.src);
sceneSprite.width = sceneWidth;
sceneSprite.height = sceneHeight;
container.addChild(sceneSprite);
// add drawings.
for(let drawingObject of canvas.drawings.placeables){
    const shape = drawingObject.hasText ? new PreciseText(drawingObject.text.text, drawingObject.text.style) 
                                        : new PIXI.Graphics(drawingObject.shape.geometry);
    shape.position.x = drawingObject.x - sceneX + (drawingObject.hasText ? drawingObject.text.x : 0);
    shape.position.y = drawingObject.y - sceneY + (drawingObject.hasText ? drawingObject.text.y : 0);
    container.addChild(shape)
}
//add tiles
for(let tile of canvas.tiles.placeables) {
    const tileSprite = PIXI.Sprite.from(tile.document.texture.src);
    tileSprite.alpha = tile.mesh.alpha
    tileSprite.width = tile.mesh.width;
    tileSprite.height = tile.mesh.height;
    tileSprite.x = tile.x - sceneX;
    tileSprite.y = tile.y - sceneY;
    container.addChild(tileSprite);
}
//add pins
for(let pinObject of canvas.notes.placeables) {
    const pinSprite = PIXI.Sprite.from(pinObject.controlIcon.icon.texture);
    pinSprite.width = pinObject.width;
    pinSprite.height = pinObject.height;
    pinSprite.x = pinObject.x - sceneX - pinSprite.width / 2;
    pinSprite.y = pinObject.y - sceneY - pinSprite.height / 2;
    container.addChild(pinSprite);
}
const fileName = `image-${canvas.scene.name}.webp`; // edit to your preference.
const folder = "/assets/"                           // an existing folder in your Data folder.
const texture = canvas.app.renderer.generateTexture(container);
const sprite = new PIXI.Sprite(texture);
const toCreate = await ImageHelper.createThumbnail(sprite, {width: container.width, height: container.height,  format:"image/webp", quality: 1});
await ImageHelper.uploadBase64(toCreate.thumb, fileName, folder, {type: "image/webp"});