function handleEvents(_originalEvent, html) {
    html.addEventListener("change", function (event) {
        const formula = html.querySelector("[name=formula]");
        if (event.target.name === "diceCount") {
            const count = event.target.value;
            const oldFormula = formula.value;
            const newFormula = oldFormula.replace(/\d{1,3}(?=d)/gm, count);
            formula.value = newFormula;
        }
        if (event.target.name === "faces") {
            const faces = event.target.value;
            const oldFormula = formula.value;
            const newFormula = oldFormula.replace(/(?<=d)\d{1,3}/gm, faces);
            formula.value = newFormula;
        }
        // look into using signed strings for the constant.
        if (event.target.name === "bonus") {
            const input = Number(event.target.value);
            console.log(input)
            const sign = input >= 0 ? "+" : "-";
            const value = Math.abs(input);
            const oldFormula = formula.value;
            const newFormula = oldFormula.replace(/[+,-]/gm, sign).replace(/\s\d{1,3}/gm, " " + value);
            formula.value = newFormula;
        }
    });
}

const { DialogV2 } = foundry.applications.api;
const { NumberField, StringField } = foundry.data.fields;

const amountField = new NumberField({
    label: "Dice Count:"
}).toFormGroup({}, { name: "diceCount", value: 1, min: 1, max: 100 }).outerHTML;
const facesField = new NumberField({
    label: "Die Faces:"
}).toFormGroup({}, { name: "faces", value: 6, min: 1, max: 100 }).outerHTML;
const bonusField = new NumberField({
    label: "Bonus:"
}).toFormGroup({}, { name: "bonus", value: 0 }).outerHTML;
const formulaField = new StringField({
    label: "Formula"
}).toFormGroup({}, { name: "formula", value: "1d6 + 0" }).outerHTML;


await DialogV2.prompt({
    window: { title: "Test" },
    content: amountField + facesField + bonusField + formulaField,
    ok: {
        callback: async (_event, button) => {
            const formula = new FormDataExtended(button.form).object.formula;
            if (!Roll.validate(formula)) return ui.notifications.warn("Not a valid formula");
            await new Roll(formula).toMessage();
        }
    },
    render: handleEvents
});