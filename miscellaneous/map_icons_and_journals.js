//use this macro in conjunction with map_icons_and_journals.json from the same gitlab you got this macro from.
const { DialogV2 } = foundry.applications.api;
const { FilePathField } = foundry.data.fields;

async function makeNotes(data) {
    const parentFolder = "Geography"; // needs to be a name of a folder that exists in your Journal tab.
    const parentId = game.journal.folders.getName(parentFolder).id;
    for (let key of Object.keys(data)) {
        let folder = game.journal.folders.find(f => f.name === key && f.parent?.name === parentFolder);
        if (!folder) folder = await Folder.create({ name: key, folder: parentId, type: "JournalEntry" });
        const journalData = data[key].map(d => ({
            name: d.name,
            pages: [{ name: d.name, "text.content": d.content }],
            folder: folder.id
        }));
        const created = await JournalEntry.createDocuments(journalData);
        const noteData = created.map((n, i) => ({
            entryId: n.id,
            fontSize: 20,
            iconSize: 32,
            x: data[key][i].x,
            y: data[key][i].y,
            texture: {
                src: data[key][i].icon,
                tint: data[key][i].color
            },
            textAnchor: CONST.TEXT_ANCHOR_POINTS.CENTER
        }));
        await canvas.scene.createEmbeddedDocuments("Note", noteData);
        await new Promise(resolve => { setTimeout(resolve, 1000); });
    }
}
const fileField = new FilePathField({
    categories: ["TEXT"],
    label: "Select JSON file"
}).toFormGroup({}, { name: "path" }).outerHTML;
await DialogV2.prompt({
    window: { title: "JSON selector" },
    content: fileField,
    ok: {
        label: "ok",
        callback: async (_event, button) => {
            const path = new FormDataExtended(button.form).object.path;
            try {
                const response = await fetch(path);
                if (!response.ok) {
                    throw new Error(`Response status: ${response.status}`);
                }

                const json = await response.json();
                makeNotes(json);
            } catch (error) {
                console.error(error.message);
            }
        }
    }

});