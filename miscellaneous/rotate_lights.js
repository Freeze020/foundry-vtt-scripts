// world script part
class LightRotationFunctions {
    static _onLightRotation(objectUuid, updateObject){
        const {object} = fromUuidSync(objectUuid);
        if (!object) return;
        object.document.updateSource(updateObject);
        if(object.document.documentName === "AmbientLight") object.updateSource();
    }
}
Hooks.on("ready", () => {
    game.socket.on(`world.${game.world.id}`, request => {
        if(request.action === "pushLightRotation") {
            LightRotationFunctions._onLightRotation(request.data.uuid, request.data.updateObject);
        }
    });
});


// Macro 1. (this starts the animating, note only works when ALL players are on the same map, needs to be started, and STOP this animation before you switch maps to prevent possible errors.)
const uuid = "UUid of the light"
const light = fromUuidSync(uuid);
canvas.app.ticker.add(onTick);
const degrees = 3;

async function onTick(delta){
	const oldSettings = {rotation: light.rotation};
	if(this.stopAnimation) {
		await  light.update(oldSettings, {diff: false});
		return canvas.app.ticker.remove(onTick);
	}
	
	const updateObject = {
		"rotation": (light.rotation + degrees)%360,
	}
	light.updateSource(updateObject);
	light.object.updateSource(); // not the same source!!! as above. This deals with vision and such.
	game.socket.emit(`world.${game.world.id}`, { action: "pushLightRotation", data: { uuid,updateObject} });
}

// Macro 2. (this stops the animating, if needed for swapping the scene or if the light needs to stop rotating)
const macro = game.macros.get("id of Macro 1");
macro.stopAnimation = !macro.stopAnimation;