//v12 macro:
async function move(direction, fine) {
    let distance = fine ? 1 : 10;
    const distanceX = direction == "left" ? (distance * (-1)) : direction === "right" ? distance : 0;
    const distanceY = direction == "up" ? (distance * (-1)) : direction === "down" ? distance : 0;
    const tilesToMove = canvas.tiles.controlled ;
    const updates = tilesToMove.map(tile => ({
        _id: tile.id,
        x: tile.document.x + distanceX,
        y: tile.document.y + distanceY
    }));
    await canvas.scene.updateEmbeddedDocuments("Tile", updates);
}

async function rotate(direction, fine) {
    let updates = [];
    if(direction !== "rotate-reset") {
        let increment = fine ? 1 : 15;
        increment = direction === "rotate-right" ? increment : increment * -1;
        const tilesToMove = canvas.tiles.controlled;
        updates = tilesToMove.map(tile => ({
            _id: tile.id,
            rotation: (tile.document.rotation + increment)%360
        }));
    }
    else {
        const tilesToMove = canvas.tiles.controlled;
        updates = tilesToMove.map(tile => ({
            _id: tile.id,
            rotation: 0
        }));  
    }
    await canvas.scene.updateEmbeddedDocuments("Tile", updates);
}

async function handleEvents(html) {
    html.find("a.direction-button").click(function(){
        
    });
}
//background-color: #bdbdbd8f;
// border-radius: 3px;
// line-height: 40px;
// border: 2px groove var(--color-border-light-highlight);
const style = `<style>
    #tile-mover-dialog .direction-button-box{
      display: flex;
      justify-content: space-evenly;
    }
    #tile-mover-dialog .direction-button {
      font-size: 2em;
      display: flex;
      text-align: center;
      height: 40px;
      justify-content: center;
    }
    #tile-mover-dialog .direction-fine  {
      min-width: 150px;
    }
</style>`;

const content = style + `<fieldset><legend>Move</legend><div class="direction-button-box">
    <a class="direction-button button" data-direction="left"><i class="fas fa-long-arrow-alt-left"></i></a>
    <a class="direction-button button" data-direction="up"><i class="fas fa-long-arrow-alt-up"></i></a>
    <a class="direction-button button" data-direction="down"><i class="fas fa-long-arrow-alt-down"></i></a>
    <a class="direction-button button" data-direction="right"><i class="fas fa-long-arrow-alt-right"></i></a>
</div></fieldset>
<fieldset><legend>Rotate</legend><div class="direction-button-box">
    <a class="direction-button button" data-direction="rotate-left"><i class="fas fa-undo"></i></a>
    <a class="direction-button button" data-direction="rotate-reset"><i class="fab fa-creative-commons-zero"></i></a>
    <a class="direction-button button" data-direction="rotate-right"><i class="fas fa-redo"></i></a>
</div></fieldset>
<div class="form-group">
    <label class="direction-fine">Fine control?</label>
    <div class="form-fields">
      <input name="control" type="checkbox" checked/>
  </div>
</div>
<p class="hint">Fine <i class="fas fa-arrows-alt"></i>: 1 pixel, <i class="fas fa-redo"></i>: 1 degrees</p>
<p class="hint">Normal <i class="fas fa-arrows-alt"></i>: 10 pixels, <i class="fas fa-redo"></i>: 15 degrees</p>`;

let d = new foundry.applications.api.DialogV2({
    window: {title: "Tile mover"},
    content,
    buttons: [
        {
          label: "Close",
          action: "close"
        }
    ],
    render: handleEvents,
    id: "tile-mover-dialog",
    position: {
      width: 250,
      left:0
    }
});
d.addEventListener("render", ()=>{
  const html = d.element;
  html.querySelectorAll(".direction-button").forEach(e => e.addEventListener("click", function(event){
    const fine = html.querySelector("input[name=control]").checked;
    if(this.dataset.direction.includes("rotate")) rotate(this.dataset.direction, fine);
    else move(this.dataset.direction, fine);
  }));
});
d.render(true);