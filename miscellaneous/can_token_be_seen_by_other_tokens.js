const tokenA = canvas.tokens.get("id of token you are interested in");
const allTokens = canvas.tokens.placeables.filter(e=> e.id !== tokenA.id); // remove token of interest

function detect(sources, target){
  const total = [];
  for(const source of sources){
    const sourceDetectionModes = source.document.detectionModes;
    const visionData = source._getVisionSourceData();
    visionData.object = source;
    const sourceVisionSource = new CONFIG.Canvas.visionSourceClass(visionData);
    foundry.utils.mergeObject(sourceVisionSource.data, source.center);
    sourceVisionSource.initialize();
    const results = [];
    for(const mode of sourceDetectionModes){
      const {tests} = canvas.visibility._createVisibilityTestConfig(target.center,{object:target});
      results.push(CONFIG.Canvas.detectionModes[mode.id].testVisibility(sourceVisionSource, mode, {object: target, tests}));
    }
    total.push(results.some(e=>e));
  }
  return total.some(e=>e);
}

const isDetected = detect(allTokens, tokenA);
console.log(isDetected);