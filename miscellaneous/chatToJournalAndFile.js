//// v10/v11/v12 version////
if (!game.user.isGM) return;
const journalName = "Your Journal Name HERE"; // Change that name to the journal name you want your sessions to be stored in.

const date = new Date().toDateString();
const pageName = `Session ${date}`;
const fileName = `fvtt-log-${date.replace(/\s/g, "-")}.txt`;
const journal = game.journal.getName(journalName) ?? await JournalEntry.create({ name: journalName });

const entry = journal.pages.find(e => e.name === pageName);
if (!entry) {
    let messages = game.messages.filter(m => !m.rolls.length && !m.content.includes("<div "));
    let pageContent = "";
    let fileContent = "";
    for (let message of messages) {
        let time = new Date(message.timestamp).toLocaleDateString(`en-GB`, { weekday: "short", hour: "numeric", minute: "numeric", second: "numeric" });
        let messageAuthor = message.speaker.alias !== undefined ? message.speaker.alias : game.user.name;
        let messageContent = message.content;
        pageContent += `<p>[${time}] ${messageAuthor}:</p><p>${messageContent}</p><p>--------------------------</p>`;
        fileContent += `[${time}] ${messageAuthor}:\n${messageContent}\n--------------------------\n`;
    }
    await JournalEntryPage.create({ name: pageName, text: { content: pageContent, format: 1 }, title: { show: false, level: 1 }, "ownership.default": 3, sort: journal.sheet._pages.at(-1)?.sort ?? 0 + CONST.SORT_INTEGER_DENSITY }, { parent: journal });
    journal.sheet.render(true);
    saveDataToFile(fileContent, "text/plain", fileName);
}
else return ui.notifications.warn(`Journal Page - ${pageName} already exists`);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
//// v9 version ////
const folderName = "Chatlog repository" // name this something unique for your world.
let folder = game.folders.getName(folderName);
if(!folder) {
    [folder] = await Folder.createDocuments([{name: folderName, type: "JournalEntry"}])
}
const folderId = folder.id;
const date = new Date().toDateString();
const journalName = `Session ${date}`;
const fileName = `fvtt-log-${date.replace(/\s/g, "-")}.txt`;
let entry = game.journal.entities.find(e => e.name === journalName);
if(!entry) {
    let messages = game.messages.contents.filter(m => !m.data.roll && !m.data.content.includes("<div "));
    let journalContent = "";
    let fileContent = "";
    for (let message of messages) {
        let time = new Date(message.data.timestamp).toLocaleDateString(`en-GB`, {weekday: "short", hour: "numeric", minute: "numeric", second: "numeric"});
        let messageAuthor = message.data.speaker.alias !== undefined ? message.data.speaker.alias : "GM";
        let messageContent = message.data.content;
        journalContent += `<p>[${time}] ${messageAuthor}:</p><p>${messageContent}</p><p>--------------------------</p>`;
        fileContent += `[${time}] ${messageAuthor}:\n${messageContent}\n--------------------------\n`;
    }
    await JournalEntry.create({folder: folderId, name: journalName, content: journalContent, "permission.default": 3}, {renderSheet: true});
    saveDataToFile(fileContent, "text/plain", fileName);
}
else return ui.notifications.warn(`Journal - ${journalName} already exists`);
*/
