//This example outputs several roll results in one ChatMessage.
//just define your rolls, and define your flavor that belongs with it, and let the good times roll :)
//functions in v10-v12

async function makeChatFromRolls(data) {
    const rolls = await Promise.all(data.map(d => new Roll(d.formula).evaluate()));
    const rollArrayResults = rolls.map(d => d.total);
    const rollHTMLs = await Promise.all(rolls.map(e => e.render()));
    const content = rollHTMLs.reduce((acc, e, i) => acc + `<br><div>${data[i].flavor}</div>${e}`, "");
    await Promise.all(rolls.map(e => game.dice3d?.showForRoll(e)))
    await ChatMessage.create({ content });
    return rollArrayResults;
}
//lets try it with a stat array for a 5e character.
const results = await makeChatFromRolls([
    { formula: "4d6dl1", flavor: "Roll 1:" },
    { formula: "4d6dl1", flavor: "Roll 2:" },
    { formula: "4d6dl1", flavor: "Roll 3:" },
    { formula: "4d6dl1", flavor: "Roll 4:" },
    { formula: "4d6dl1", flavor: "Roll 5:" },
    { formula: "4d6dl1", flavor: "Roll 6:" }
]);